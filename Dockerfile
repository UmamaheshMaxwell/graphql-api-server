# Use a Node.js base image with version 20
FROM node:20

# Set the working directory inside the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code to the working directory
COPY . .

# Expose the port on which the server will run (8080)
EXPOSE 8080

# Command to run the server when the container starts
CMD ["node", "server.js"]
