import { gql } from 'apollo-server';
/*  
    ? Datatypes Available
    * int, float, string, boolean, ID

*/
export const typeDefs = gql`
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

  type Game {
    id: ID!
    title: String!
    platform : [String!]!
    reviews: [Review!]
  }

  type Review {
    id: ID!
    rating: Int!
    content: String!
    game: Game!
    author: Author!
  }

  # This "Book" type defines the queryable fields: 'title' and 'author'.
  type Book {
    title: String
    author: String
  }

  type Author {
    id: ID!
    name: String!
    verified: Boolean!
    reviews: [Review!]
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  # case, the "books" query returns an array of zero or more Books (defined above).
  type Query {
    books: [Book]

    games: [Game]
    game(id:ID!): Game

    reviews: [Review]
    review(id:ID!): Review

    authors: [Author]
    author(id:ID!): Author
  }
`;
