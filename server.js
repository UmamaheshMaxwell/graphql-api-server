import { ApolloServer } from 'apollo-server';
import { typeDefs } from './typedefs.js';
import { resolvers } from './resolvers.js';

const PORT = 8080; // Specify the port number

const server = new ApolloServer({ typeDefs, resolvers })
// The `listen` method launches a web server.
server.listen({ port: PORT }).then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});