import db from './db.js';

export const resolvers = {
    Query: {
        games: () => db.games,
        reviews: () => db.reviews,
        authors: () => db.authors,

        game: (_, args) => {
            return db.games.find(game => game.id === args.id)
        },
        review: (_, args) => {
            return db.reviews.find(review => review.id === args.id)
        },
        author: (_, args) => {
            return db.authors.find(author => author.id === args.id)
        }
    },
    Game: {
        reviews: (parent) => db.reviews.filter(review => review.game_id === parent.id)
    },

    Author: {
        reviews: (parent) => db.reviews.filter(review => review.author_id === parent.id)
    }
};